import sqlite3

class database():
    '''
    Provide an interface to the database for a ITEC831 web application
    + Port of my AWESOME PHP db functions :^)
    '''

    def __init__(self, dbname="website.db"):
        '''
        Constructor, database name is an optional parameter
        the default 'comp249.db' is suitable for most cases.
        '''
        
        self.dbname = dbname
        self.conn = sqlite3.connect(self.dbname)
        ### ensure that results returned from queries are strings rather
        ### than unicode 
        self.conn.text_factory = str
        self.conn.row_factory = sqlite3.Row
        self.initialize()


    def initialize(self):
        """On first run, user may start with an empty database, so this checks if database must be recreated from dump file"""
        res = self.get_one("SELECT name FROM sqlite_master WHERE type='table' AND name='user'");
        if not res:
            self.reset()

    def reset(self):
        file = open('dump.sql', 'r')
        self.conn.executescript(file.read())
        
    def cursor(self):
        """Return a cursor on the database"""
        
        return self.conn.cursor()
    
    def commit(self):
        """Commit pending changes"""
        
        self.conn.commit()

    def get_one(self, sql, params=()):
        """Return a single scalar value"""
        cursor = self.execute(sql, params)
        row = cursor.fetchone()
        return row[0] if row else None

    def get_row(self, sql, params=()):
        """Fetches a single row"""
        cursor = self.execute(sql, params)
        res = cursor.fetchone()
        #print( self.__class__.__name__ + " "  +str(res))
        return res

    def get_all(self, sql, params=()):
        """Fetches some iterable with all rows of a query result. Empty list if no rows"""
        c = self.execute(sql, params)
        return c.fetchall()

    def insert(self, tablename, values):
        t1 = []
        t2 = []
        for k, v in values.items():
            t1.append(k)
            if not v:
                t2.append("NULL")
            else:
                t2.append(v)
            #t2.append(v if v else "NULL")

        sql = "INSERT INTO %s (%s) VALUES (%s)" % (tablename, ', '.join(t1), ', '.join( ('?',)* len(t2)  ))

        c = self.execute(sql, t2)
        self.commit()
        return c.lastrowid

    def update(self, tablename, pairs, where='', params=()):

        sql = "UPDATE %s SET " % tablename
        values = []
        for k, v in pairs.items():
            values.append( "%s = ?" % k  )
        values = ", ".join(values)
        sql = sql + values + " WHERE "   + where
        self.execute(sql, list(pairs.values()) + list(params) )
        self.commit()

    def delete(self, table, where, params=()):
        sql = "DELETE FROM %s WHERE %s" % (table, where)
        self.execute(sql, params)
        self.commit()

    def execute(self, sql, params=()):
        # if isinstance(params, str):
        #     params = (params)
        print('DB:' + sql + " - " + str(params))
        cursor = self.cursor()
        cursor.execute(sql, params)
        return cursor



