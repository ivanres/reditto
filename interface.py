#from collections import OrderedDict
import hashlib

def get_posts(db, q=False, params=()):
    sql = """SELECT p.*, u.username
                        FROM post p
                        INNER JOIN user u ON p.user_id=u.id
                      """
    if q:
        sql += q
    rows = db.get_all(sql, params)
    res = []
    for row in rows:
        p = dict(row)
        p['votes'] = get_votes(db, 'post', row['id'])
        res.append(p)

    res = sort_by_votes(res)

    return res

def get_votes(db, type, id):
    return int(db.get_one('SELECT total(vote) FROM vote WHERE item_type=? AND item_id=?', (type, id)))

def get_comments(db, post_id):
    rows = db.get_all("""SELECT c.*, u.username
                        , COALESCE(u.avatar, 'default.gif' ) as avatar
                        FROM comment c
                        INNER JOIN user u ON u.id=c.user_id
                        AND c.type='post' AND c.parent_id=?
                        """, (post_id, ))
    res = []
    for row in rows:
        c = dict(row)
        c['votes'] = get_votes(db, 'comment', c['id'])
        c['comments'] = get_replies(db, c['id'])
        res.append(c)

    res = sort_by_votes(res)

    return res

def get_replies(db, id):
    rows = db.get_all("""SELECT c.*, u.username
                        , COALESCE(u.avatar, 'default.gif' ) as avatar
                        FROM comment c
                        INNER JOIN user u ON u.id=c.user_id
                        AND c.type='comment' AND c.parent_id=?
                        """, (id, ))
    res = []
    for row in rows:
        c = dict(row)
        c['votes'] = get_votes(db, 'comment', c['id'])
        c['comments'] = get_replies(db, c['id'])
        res.append(c)

    res = sort_by_votes(res)

    return res

def sort_by_votes(d):
    """Higher voted posts are then displayed higher"""
    return sorted(d, key=lambda x: x['votes'], reverse=True)

def get_tags(db, post_id):
    return db.get_all("SELECT tag_id FROM post_tag WHERE post_id=?", (post_id, ))

def search_posts(db, q):
    return get_posts(db, " WHERE p.title LIKE ? ", ( "%" + q + "%" ,) )

def encrypt(pwd):
    return hashlib.sha1(pwd.encode('utf-8')).hexdigest()
