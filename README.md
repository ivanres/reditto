# Reditto #

## Description ##

Reditto is a content aggreggator like Reddit or Digg, written in python using the Bottle web framework.



## Features ##
- Users can sign up, log in and log out.
- Text, link and images can be submitted, with a text body.
- Users can vote on posts (up or down). Higher voted posts are then displayed higher
- Posts can be commented on by other users
- Post comments can be replied to by others
- Post comments can also be voted on (up or down)
- Users have avatars
- Posts can be tagged with hashtags or keywords
- Search field

##  How to run ##

From the project root, just run

`python server.py`