from bottle import *
from db import database
from interface import *
from session import Session
import os

@get('/')
def index():
    posts = get_posts(db)
    return template('index.tpl', posts=posts, user=get_user(), flash=session.get_flash())

def get_user():
    user = db.get_row("SELECT * FROM user WHERE id=?", (session.get_user_id(),)) if session.get_user_id() else None
    if user:
        user = dict(user)
        user['avatar'] = user['avatar'] if user['avatar'] else 'default.gif'
    return user

@route('/login', method='GET')
def login():
    return template('login.tpl')

@route('/register', method='POST')
def do_register():
    try:
        username = request.forms.get('username')
        password = request.forms.get('password')
        password_again = request.forms.get('password_again')

        if not username or not password or not password_again:
            raise Exception("Please fill in all the form fields.")

        #Some more validation
        if password != password_again:
            fail("Passwords do not match.")

        if db.get_one("SELECT username FROM user WHERE username=?", (username, )):
            fail("Username already exists")

        db.insert("user", {'username': username, 'password': encrypt(password)})
        log_user(username, password)
    except Exception as e:
        return template('login.tpl', error=get_error(e))

    redirect('/')

def get_error(e):
    try:
        return e.args[0]
    except:
        return "Unknown error"

@route('/login', method='POST')
def do_login():
    try:
        username = request.forms.get('username')
        password = request.forms.get('password')
        if not username or not password:
            raise Exception("Please fill in all the form fields.")
        log_user(username, password)
    except Exception as e:
        return template('login.tpl', error=get_error(e))

    dest = '/'
    # try:
    #     dest = request.query.dest
    # except:
    #     pass

    redirect(dest) ##All fine

def log_user(username, password):
    user = find_user(username, password)
    #print(str(user))
    if not user:
        raise Exception("Invalid credentials.")
    session.new_session()
    session.set_user_id(user['id'])
    return user

def find_user(username, password):
    return db.get_row("SELECT * FROM user WHERE username=? AND password=?", (username, encrypt(password)))

@get('/logout')
def logout():
    session.destroy() #The cookie is deleted. However, since I redirect to the homepage, it generates again with a NEW value.
    return template('logged_out.tpl')

@get('/submit')
def submit():
    user = get_user()
    if not user:
        session.set_flash("You must be logged in to post")
        redirect('/')
    else:
        return template('submit.tpl', user=user)

@post('/submit/<type>')
def do_submit(type):
    try:

        user = get_user()
        if not user:
            fail("Please log in")

        title = request.forms.get('title')
        link = request.forms.get('link')
        text = request.forms.get('text')
        upload = request.files.get('upload')
        tags = request.forms.get('tags')
        if not title:
            fail("Title is required")

        if type not in ('link', 'text', 'image'):
            fail("Unsupported type")

        post = {'user_id': user['id'], 'title': title, 'type': type}
        if type == 'link':
            if not link:
                fail("Link is required")
            post['body'] = link
        elif type == 'text':
            if not text:
                fail("Text is required")
            post['body'] = text
        else:
            #Assume upload
            name, ext = os.path.splitext(upload.filename)
            if ext not in ('.png','.jpg','.jpeg', '.gif'):
                fail('Only images are allowed.')

            save_path = upload_path()
            upload.save(save_path) # appends upload.filename automatically
            post['body'] = upload.filename

        post_id = db.insert("post", post)
        handle_tags(post_id, tags)

    except Exception as e:
        #session.set_flash(get_error(e))
        #redirect('/submit')
        return template('submit.tpl', error=get_error(e), user=user)

    session.set_flash("Post created!")
    redirect('/')

def upload_path():
    return os.path.dirname(os.path.realpath(__file__)) + '/static/uploads' #Hmmmmm

@get('/uploads/<filename:re:.*\.(jpg|jpeg|png|gif)>')
def uploads(filename):
    return static_file(filename, root='static/uploads')

def handle_tags(post_id, tags):
    if not tags:
        return
    tags = tags.split()
    if not tags:
        return

    for tag in tags:
        tag = tag.strip()
        if not tag: continue
        try:
            db.insert('tag', {'id':tag})
        except:
            pass

        db.insert('post_tag', {'post_id': post_id, 'tag_id': tag})


@get('/post/<id>')
def post_view(id):
    #xtodo validation
    post = db.get_row("""
    SELECT post.*, username
    FROM post
    INNER JOIN user ON post.user_id=user.id AND post.id=?
    -- LEFT JOIN votes ON
    """, (id, ))


    if not post:
        abort(404, 'Post does not exist')

    post = dict(post)
    post['comments'] = get_comments(db, post['id'])
    post['tags'] = get_tags(db, post['id'])

    return template('post.tpl', flash=session.get_flash(), user=get_user(),  **post)



@post('/post/<id>/vote/<amount>')
def vote(id, amount):
    try:
        user = get_user()
        if not user:
            fail("Please log in")

        add_vote(user, 'post', id, amount)

    except Exception as e:
        session.set_flash(get_error(e))

    session.set_flash('Vote accepted!')
    redirect('/')



@post('/comment/<id>/vote/<amount>')
def comment_vote(id, amount):
    try:
        user = get_user()
        if not user:
            fail("Please log in")

        add_vote(user, 'comment', id, amount)

    except Exception as e:
        session.set_flash(get_error(e))
        return ''

    session.set_flash('Vote accepted!')
    #redirect('/')

def add_vote(user, type, id, amount):
    if amount not in ('1','-1'):
        fail("Invalid vote ")

    try:
        db.insert('vote', {'user_id': user['id'], 'item_id': id, 'item_type': type, 'vote':amount})
    except:
        fail("You can only vote once")

def fail(msg):
    raise Exception(msg)

@post('/post/<id>/delete')
def post_delete(id):
    try:
        user = get_user()
        if not user:
            fail("Please log in")

        #Authorized?
        post = db.get_row("SELECT * FROM post WHERE id=? AND user_id=?", (id, user['id']))

        if not post:
            fail("You are not the creator of that post!")

        if post['type'] == 'image':
            #fail("Will delete " + upload_path() + post['body'])
            os.remove(upload_path() + os.sep + post['body'])

        db.delete('post', " id=? AND user_id=?", (id, user['id']))
        #Should I delete posts and votes too??

        session.set_flash("Post deleted!")
    except Exception as e:
        session.set_flash(get_error(e))

    redirect('/')

@post('/post/<post_id>/comment')
def post_comment(post_id):
    print("post_comment")
    try:
        user = get_user()
        if not user:
            fail("Please log in")

        body = request.forms.get('body')

        if not body or not body.strip():
            fail("Message can't be empty")

        db.insert('comment', {'user_id': user['id'], 'type': 'post', 'body': body.strip(), 'parent_id': post_id })

        session.set_flash("Comment added!")
    except Exception as e:
        print(get_error(e))
        session.set_flash(get_error(e))

    redirect('/post/%s' % (post_id,))


@post('/comment/<id>/reply')
def comment_reply(id):
    post_id = request.forms.get('post_id')
    try:
        user = get_user()
        if not user:
            fail("Please log in")

        body = request.forms.get('body')
        if not body or not body.strip():
            fail("Message can't be empty")

        db.insert('comment', {'user_id': user['id'], 'type': 'comment', 'body': body.strip(), 'parent_id': id })

        session.set_flash("Comment added!")
    except Exception as e:
        print(get_error(e))
        session.set_flash(get_error(e))

    redirect('/post/%s' % (post_id,))


@get('/profile')
def profile():
    user = get_user()
    if not user:
        session.set_flash("Please log in")
        redirect('/login')
    return template('profile.tpl', user=user, **user)

@get('/avatars/<filename:re:.*\.(jpg|jpeg|png|gif)>')
def avatars(filename):
    return static_file(filename, root='static/avatars')

@post('/profile/avatar')
def profile_set_avatar():
    user = get_user()
    if not user:
        session.set_flash("Please log in")
        redirect('/login')

    #Handle upload
    try:
        upload = request.files.get('upload')
        if not upload:
            fail("Missing upload")
        name, ext = os.path.splitext(upload.filename)
        if ext not in ('.png','.jpg','.jpeg', '.gif'):
            fail('Only images are allowed.')

        upload.save(avatar_path()) # appends upload.filename automatically
        db.update('user', {'avatar': upload.filename}, " id=?", (user['id'], ))
    except Exception as e:
        return template('profile.tpl', error=get_error(e), user=user, **user)

    session.set_flash('Avatar uploaded!')
    redirect('/profile')

def avatar_path():
    return os.path.dirname(os.path.realpath(__file__)) + '/static/avatars/' #Hmmmmm

@get('/search')
def search():
    q = request.query.q

    posts = []

    if q:
        posts = search_posts(db, q)

    return template('search-results.tpl', posts=posts, user=get_user(), q=q)

@route('/static/<filename>')
def server_static(filename):
    return static_file(filename, root='static/')

@hook('before_request')
def state():
    session.start()

if __name__ == '__main__':
    db = database('website.db')
    session = Session(db, request, response)
    #session.destroy()
    run(debug=True, host='localhost', port=8080)


    
