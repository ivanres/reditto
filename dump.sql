PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE `user` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`username`	TEXT NOT NULL UNIQUE,
	`password`	TEXT,
	`display_name`	TEXT,
	`avatar`	TEXT
);
INSERT INTO "user" VALUES(1,'ivan','a15f8b81a160b4eebe5c84e9e3b65c87b9b2f18e',NULL,'ren_hoek_running.gif');
INSERT INTO "user" VALUES(2,'foo','0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33',NULL,NULL);
INSERT INTO "user" VALUES(3,'sam','f16bed56189e249fe4ca8ed10a1ecae60e8ceac0',NULL,'polar_bear.gif');
CREATE TABLE `post` (
	`id`	INTEGER,
	`title`	TEXT,
	`body`	TEXT,
	`type`	TEXT,
	`user_id`	INTEGER,
	PRIMARY KEY(id)
);
INSERT INTO "post" VALUES(1,'Blog','This framework sucks','text',1);
INSERT INTO "post" VALUES(2,'Plagiarism!!','http://www.reddit.com/','link',1);
INSERT INTO "post" VALUES(3,'Announcement','I love my new quiz system!1','text',3);
INSERT INTO "post" VALUES(4,'Google','http://www.google.com','link',1);
INSERT INTO "post" VALUES(5,'<script>alert("hi")</script> ','<script>alert("test")</script> ','text',1);
CREATE TABLE `tag` (
	`id`	TEXT,
  PRIMARY KEY(id)
);
INSERT INTO "tag" VALUES('win');
INSERT INTO "tag" VALUES('god');
INSERT INTO "tag" VALUES('success');
INSERT INTO "tag" VALUES('yes');
CREATE TABLE `post_tag` (
	`post_id`	INTEGER,
	`tag_id`	TEXT
);
INSERT INTO "post_tag" VALUES(1,'win');
INSERT INTO "post_tag" VALUES(1,'god');
INSERT INTO "post_tag" VALUES(1,'success');
INSERT INTO "post_tag" VALUES(2,'win');
INSERT INTO "post_tag" VALUES(2,'god');
INSERT INTO "post_tag" VALUES(2,'success');
INSERT INTO "post_tag" VALUES(2,'yes');
CREATE TABLE "session" (
	`id`	TEXT,
	`user_id`	INTEGER NOT NULL,
	`flash`	TEXT,
	PRIMARY KEY(id)
);

CREATE TABLE "vote" (
	`user_id`	INTEGER,
	`item_id`	INTEGER,
	`item_type`	TEXT,
	`vote`	INTEGER,
	PRIMARY KEY(user_id,item_id,item_type,vote)
);

CREATE TABLE "comment" (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`user_id`	INTEGER NOT NULL,
	`type`	TEXT NOT NULL,
	`body`	TEXT NOT NULL,
	`parent_id`	TEXT NOT NULL
);

DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('user',3);
COMMIT;
