% rebase('layout.tpl', title='Login or Register')

<h1>Register</h1>
% if defined('error'):
    <div class="error">{{error}}</div>
% end
<div>
    <h2>Create a new account</h2>
    <form action="/register" method="POST">
    <p><input type='text' name='username' placeholder='Username'/></p>
    <p><input type='password' name='password' placeholder='Password'/></p>
    <p><input type='password' name='password_again' placeholder='Confirm Password'/></p>
    <input type='submit' value="Register"/>
    </form>
</div>