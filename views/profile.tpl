% rebase('layout.tpl', title='Profile')

<h1>{{username}}'s Profile</h1>
% if defined('error'):
    <div style="color:red">{{error}}</div>
% end

<div>
    <img class="avatar" src="/avatars/{{avatar}}">
</div>

<div class="submit_form">
    <h2>Set your avatar</h2>
    <form action="/profile/avatar" method="post" enctype="multipart/form-data">
        File: <input type="file" name="upload"><br/>
        <button>Submit</button>
    </form>
</div>

