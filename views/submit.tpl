% rebase('layout.tpl', title='Submit')

<h1>Submit new content</h1>
% if defined('error'):
    <div style="color:red">{{error}}</div>
% end

<p>Use the appropriate form below to submit your content</p>

<div class="submit_form">
    <h2>Submit Text</h2>
    <form action="/submit/text" method="post">
        Title <input type="text" name="title"><br/>
        Text <textarea name="text" rows="5"></textarea><br/>
        Tags <input type="text" name="tags" placeholder="Optional"><span class="info">List of tags separated by spaces</span><br/>
        <button>Submit</button>
    </form>
</div>


<div class="submit_form">
    <h2>Submit Link</h2>
    <form action="/submit/link" method="post">
        Title <input type="text" name="title"><br/>
        Link <input type="text" name="link"><br/>
        Tags <input type="text" name="tags" placeholder="Optional"><span class="info">List of tags separated by spaces</span><br/>
        <button>Submit</button>
    </form>
</div>

<div class="submit_form">
    <h2>Submit Image</h2>
    <form action="/submit/image" method="post" enctype="multipart/form-data">
        Title <input type="text" name="title"><br/>
        File <input type="file" name="upload"><br/>
        Tags <input type="text" name="tags" placeholder="Optional"><span class="info">List of tags separated by spaces</span><br/>
        <button>Submit</button>
    </form>
</div>