% rebase('layout.tpl', title='Logged Out')

<h1>Logged out</h1>
% if defined('error'):
    <div style="color:red">{{error}}</div>
% end
<div>
    <p>You have been logged out. Click <a href="/">here</a> to continue</p>
</div>

<div class="comments">After logging out, usually you would be redirected to the front page, but this page was added so
it can be verified that the cookie has been deleted. The home page will create a cookie again to decide whether to draw
the login box or the greeting.</div>