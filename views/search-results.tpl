% rebase('layout.tpl', title='Search Results.')

<h1>Search Results</h1>
% if not posts:
    <p>No results</p>
% end
% include('posts.tpl', posts=posts )