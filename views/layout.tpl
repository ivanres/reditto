<!DOCTYPE html>
<html>
<head>
    <title>{{title if defined('title') else "Reditto"}}</title>
    <link rel="stylesheet" type="text/css" href="/static/style.css">
    <script src="/static/jquery-1.11.3.min.js"></script>
</head>
<body>
<div id="wrapper">

<div id="search">
    <form action="/search">
        % setdefault('q', '')
        <input name="q" value="{{q}}"><button>Search</button>
    </form>
</div>

<ul id="nav">
    <li><a href="/">Home</a></li>
    <li><a href="/submit">Submit</a></li>
    % if defined('user') and user:
    <li><a href="/profile">Profile</a></li>
    <li><a href="/logout">Logout</a></li>
    % else:
    <li><a href="/login">Register</a></li>
    % end
</ul>

<div id="user">
% if defined('user') and user:
    <p>Welcome {{user['username']}} <a href="/logout">logout</a></p>
% else:
    %  include('login-form.tpl')
    <!-- <a href='/register'>register</a> -->
% end
</div>



% if defined('flash') and flash:
    <div class="error">{{flash}}  </div>
% end

    {{!base}}


<div id="footer">Copyright &copy; 2015 - Ivan Rodriguez Asqui (Student Id: 43365914)</div>

</div>
</body>
</html>