        <div class="comment-box" style="margin-left: {{level*20}}px">

            <table>
                <tr>
                    <td align="center">
                        <img class="post-avatar" src="/avatars/{{avatar}}">
                        <p class="author">{{username}}</p>
                    </td>
                    <td>
                        <p>{{body}}</p>


                        <ul>
                            <li>Votes: {{votes}}</li>
                            <li><a class="vote" href="/comment/{{id}}/vote/1">Vote up</a></li>
                            <li><a class="vote" href="/comment/{{id}}/vote/-1">Vote down</a></li>
                            <li><a class="reply" href="javascript:void(0)">Reply</a>
                                <div class="hidden reply-box">
                                    <form action="/comment/{{id}}/reply" data-id="{{id}}" method="post">
                                        <input type="hidden" name="post_id" value="{{post_id}}">
                                        <textarea name="body"></textarea>
                                        <button class="save_btn">Save</button>
                                        % #<button class="cancel_btn">Cancel</button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>






            % for c in comments:
                %  include('comment.tpl', post_id=post_id, level = level + 1, **c )
            % end
        </div>