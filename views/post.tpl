% rebase('layout.tpl', title='Submit')
% post_id = id
<h1>{{title}}</h1>
<div class="author">Submitted by {{username}}</div>
<div class="body">
    % if type=='image':
        <img class="post-image" src="/uploads/{{body}}">
    % elif type=="link":
        <a href="{{body}}">{{body}}</a>
    % else:
        {{body}}
    % end
</div>

%if defined("tags") and tags:

<h2>Tags:</h2>
<ul class="tag-list">
% for tag in tags:
    <li>{{tag['tag_id']}}</li>
% end
</ul>

%end

<input type="hidden" id="post_id" value="{{post_id}}">

% if user and user['id'] == user_id:
    %  include('delete-form.tpl', id=id )
% end
<br>
<h2>Comments</h2>
% if user:
    <form method="post" action="/post/{{id}}/comment">
    <textarea name="body" placeholder="Add new comment"></textarea>
    <button>Save</button>
    </form>
% end

% if not comments:
    <p>No comments</p>
% else:
    % for c in comments:
        %  include('comment.tpl', post_id=post_id, level=0, **c )
    % end
% end

<script>
$('a.reply').click(function(){
    console.log('clicked')
    var u = $(this).closest('li').find('.reply-box');
    u.toggle();
    if (u.is(':visible')){
        u.find('textarea').focus();
    }
});

$('a.vote').click(function(){
    var url = $(this).attr('href')
    var post_id = $('#post_id').val();
    console.log(url)
    $.post(url, {post_id: post_id}, function(){
        location.href = '/post/' + post_id;
    });
    return false;
});


</script>