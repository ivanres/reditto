% for post in posts:
<div class="post">
    % path = '/post/%d'% post['id']
    % link = post['body']  if post['type']=='link' else path
    <h2><a href="{{link}}">{{post['title']}}</a></h3>
    <div class="author">Submitted by {{post['username']}}</div>
    <div class="votes">
        <p> Votes: {{post['votes']}} </p>
        % if user:
            % # <a href="/vote/post/{{ post['id']}}/1">Vote Up</a> | <a href="/vote/post/{{ post['id']}}/-1">Vote Down</a>
            <form method="post" action="{{path}}/vote/1">
                <button>Vote up</button>
            </form>
            <form method="post" action="{{path}}/vote/-1">
                <button>Vote down</button>
            </form>
        % end
    </div>
    % if user and user['id'] == post['user_id']:
        %  include('delete-form.tpl', id=post['id'] )
    % end
    <p><a href="{{path}}">Comments</a></p>
</div>
% end