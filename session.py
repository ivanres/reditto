import uuid

class Session:
    """Simple container of session related functions"""

    def __init__(self, db, request, response, table="session", cookie_name="REDDITO_COOKIE"):
        self.key = None
        self.db = db
        """:type: db.database"""

        self.table = table
        self.cookie_name = cookie_name

        self.request = request
        """@type response bottle.HTTPRequest"""

        self.response = response
        """@type response bottle.HTTPResponse"""

    def start(self):
        self.key = self.request.get_cookie(self.cookie_name)
        if not self.key:
            self.new_session()



    def new_session(self):
        self.key = str(uuid.uuid4())
        self.db.insert(self.table, {'id':self.key, 'user_id': -1}) #At the beginning, no identity
        self.response.set_cookie(self.cookie_name, self.key)
        return self.key

    def set_user_id(self, user_id):
        self.db.update(self.table, {'user_id': user_id}, 'id=?', (self.key,) )

    def get_user_id(self):
        if not self.key: return None
        user_id = self.db.get_one("SELECT user_id FROM %s WHERE id=?" % self.table, (self.key, ) )
        return user_id if user_id and user_id != -1 else None

    def set_flash(self, flash):
        if not self.key: return
        if not type(flash) is str:
            print("set_flash: Incorrect flash parameter")
            return
        self.db.update(self.table, {'flash': flash}, "id=?", (self.key,) )

    def get_flash(self):
        flash = self.db.get_one("SELECT flash from %s WHERE id=?" % self.table, (self.key, ) )
        if flash:
            self.set_flash('')
        return None if not flash else flash

    def destroy(self):
        self.db.delete(self.table, "id=?", (self.key,) )
        self.response.set_cookie(self.cookie_name, '', expires=-1) #Deletes the cookie
        self.key = None